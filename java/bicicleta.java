class Bicicleta extends Vehiculo{
	private int numero_ruedas;
	public Bicicleta(String modelo, int year, int ruedas){
		super(modelo,year);
		this.numero_ruedas = ruedas;
	}
	public void descripcion(){
		System.out.println("Este vehiculo es: "+super.toString()+" y tiene "+this.numero_ruedas+" rueda(s)");
	}

}