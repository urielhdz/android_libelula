class Carro extends Vehiculo {
	private int numero_ruedas;
	public Carro(String modelo, int year, int ruedas){
		super(modelo,year);
		this.numero_ruedas = ruedas;
	}
	public void descripcion(){
		System.out.println("Este carro es: "+super.toString());
	}
	
}