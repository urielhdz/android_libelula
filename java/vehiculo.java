class Vehiculo{
	private String modelo;
	private int year;
	Vehiculo(String modelo, int year){
		this.modelo = modelo;
		this.year = year;
	}

	public String toString(){
		return year + " "+ modelo;
	}
	// Que pasa si un objeto no tiene descripcion?
	public void descripcion(){
		System.out.println("Este vehiculo es: "+super.toString()+" y tiene ");
	}
}