class Vehiculo{
	private String modelo;
	private int year;

	Vehiculo(String modelo, int year){
		this.modelo = modelo;
		this.year = year;
	}
	public String toString(){
		return year + " "+ modelo;
	}
	public void descripcion(){
		System.out.println("Este Vehiculo es "+this.toString());
	}
	
}