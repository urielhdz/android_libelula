class Point implements Drawable {
	private int x,y,red,green,blue;

	Point(int x, int y){
		this.x = x;
		this.y = y;
	}
	public String toString(){
		return "x: "+this.x+" y: "+this.y;
	}
	public void draw(){
		System.out.println("Punto dibujado en: "+ toString()+ " en color: "+ getColor());
	}
	public void setColor(int[] rgb_color){
		if(rgb_color.length < 3){
			System.out.println("Datos mal formados");
			return;
		}
		this.red = rgb_color[0];
		this.green = rgb_color[1];
		this.blue = rgb_color[2];
	}
	public String getColor(){
		return "R: "+ this.red+ " G: "+ this.green+ " B: "+this.blue;
	}
}