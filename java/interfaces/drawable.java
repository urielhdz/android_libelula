interface Drawable{
	void draw();
	void setColor(int[] rgb_color);
	String getColor();
}