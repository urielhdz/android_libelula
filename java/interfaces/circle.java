class Circle extends Point implements Drawable {
	private int radio = 0;
	Circle(int x, int y,int radio){
		super(x,y);
		this.radio = radio;
	}
	public void setRadio(int radio){
		this.radio = radio;
	}
	public int getRadio(int radio){
		return this.radio;
	}
	
	public void draw(int[] rgb_color){
		System.out.println("Circulo dibujado en: "+ toString()+ " en color: "+ getColor()+" y radio: "+this.radio);
	}
}