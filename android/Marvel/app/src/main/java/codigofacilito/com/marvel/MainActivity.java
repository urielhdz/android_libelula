package codigofacilito.com.marvel;

import android.app.ListActivity;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;


public class MainActivity extends ListActivity {
    ArrayList<HashMap<String,String>> mSuperHeroes = new ArrayList<HashMap<String,String>>();
    protected ArrayList<String> mIds = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView lv = (ListView) findViewById(android.R.id.list);
        lv.setEmptyView(findViewById(R.id.vacio));

        new ObtenerSuperHeroes().execute("");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private class ObtenerSuperHeroes extends AsyncTask<String,Void,JSONArray>{
        private final String TAG = ObtenerSuperHeroes.class.getSimpleName();

        @Override
        protected JSONArray doInBackground(String... params) {


            Long ts = System.currentTimeMillis()/1000;
            String ts_string = ts.toString();
            String api_key = "205530edd3fa697705aa1a2430ae87ba";
            String private_key = "5cba4ac12e3fb326645e2f1cfadeae643aa20132";
            String hash = Utilidades.md5(ts_string+private_key+api_key);

            String final_string = "";
            int codigo_respuesta = -1;

            try{
                Uri url_construida = Uri.parse("http://gateway.marvel.com/v1/public/characters?").buildUpon()
                        .appendQueryParameter("apikey",api_key)
                        .appendQueryParameter("ts",ts_string)
                        .appendQueryParameter("hash",hash).build();

                String url_final = url_construida.toString();
                Log.d(TAG,url_final);

                URL url = new URL(url_final);

                HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
                conexion.connect();

                codigo_respuesta = conexion.getResponseCode();
                Log.d(TAG,"Respuesta del servidor: "+codigo_respuesta);

                InputStream inputStream = conexion.getInputStream();
                if (inputStream == null) final_string = null;

                StringBuffer buffer = new StringBuffer();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                String linea;
                while((linea = reader.readLine()) != null){
                    buffer.append(linea+"\n");
                }
                if (buffer.length() == 0) final_string = null;

                if(final_string != null){
                    final_string = buffer.toString();
                    Log.d(TAG,final_string);
                    JSONObject json_object = new JSONObject(final_string);
                    return json_object.getJSONObject("data").getJSONArray("results");
                }
                else
                    return null;
            }catch (Exception e){

            }

            return null;
        }
        @Override
        protected void onPostExecute(JSONArray resultado){
            try {
                updateList(resultado);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void updateList(JSONArray resultado) throws JSONException {
        if(resultado == null){

        }
        else{
            HashMap<String,String> ayuda = new HashMap<String, String>();
            for(int i = 0;i< resultado.length();i++){
                JSONObject super_heroe = resultado.getJSONObject(i);
                String nombre_super_heroe = super_heroe.getString("name");
                String descripcion_super_heroe = super_heroe.getString("description");
                ayuda = new HashMap<String, String>();
                ayuda.put("titulo",nombre_super_heroe);
                ayuda.put("descripcion",descripcion_super_heroe);

                mSuperHeroes.add(ayuda);
            }

            String[] claves = {"titulo","descripcion"};
            int[] ids = {android.R.id.text1,android.R.id.text2};

            SimpleAdapter adapter = new SimpleAdapter(this,mSuperHeroes,android.R.layout.two_line_list_item,
                                                    claves,ids);
            setListAdapter(adapter);
        }
    }
}
