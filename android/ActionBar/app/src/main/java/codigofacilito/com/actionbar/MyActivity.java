package codigofacilito.com.actionbar;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;


//Podemos extener de FragmentActivity en caso de tener compatibilidad con versiones anteriores a la 4 de Android
public class MyActivity extends FragmentActivity implements ActionBar.TabListener{
    ViewPager mViewPager;
    SectionsPageAdapter mSectionPageAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        //Configuran el ActionBar
        final ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        //Debe ser SupportFragmentManager
        //Configura el adapter que retornará el fragment para cada seccion principal de la app
        mSectionPageAdapter = new SectionsPageAdapter(this,getSupportFragmentManager());

        //Configura el ViewPager con el adapter que acabamos de configurar
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionPageAdapter);

        //Cuando hacemos swipe entre diferentes secciones, seleccionar el correspondiente TAB.
        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener(){
            @Override
            public void onPageSelected(int i){
                actionBar.setSelectedNavigationItem(i);
            }
        });

        actionBar.addTab(actionBar.newTab().setText("Super Heroes").setTabListener(this));
        actionBar.addTab(actionBar.newTab().setText("Comics").setTabListener(this));
        actionBar.addTab(actionBar.newTab().setText("Historias").setTabListener(this));


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }
}
