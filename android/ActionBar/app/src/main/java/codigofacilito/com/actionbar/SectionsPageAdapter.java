package codigofacilito.com.actionbar;

import android.support.v4.app.FragmentManager;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by urielhernandez on 20/08/14.
 */
public class SectionsPageAdapter extends FragmentPagerAdapter{

    protected Context mContext;
    public SectionsPageAdapter(Context context,FragmentManager fm){
        super(fm);
        mContext = context;

    }
    @Override
    public Fragment getItem(int i) {
        switch (i){
            case 0:
                return new SuperHeroesFragment();
            case 1:
                return new ComicsFragment();
            case 2:
                return new StoriesFragment();
            default:
                return null;

        }
    }
    //Retorna el numero de vistas disponibles
    @Override
    public int getCount() {
        return 3;
    }
}
