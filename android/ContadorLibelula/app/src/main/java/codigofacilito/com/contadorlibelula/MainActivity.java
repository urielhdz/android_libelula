package codigofacilito.com.contadorlibelula;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import java.util.Date;


public class MainActivity extends Activity implements View.OnClickListener{
    int mContador = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button boton_fecha = (Button) findViewById(R.id.boton_fecha);
        Button boton_contador = (Button) findViewById(R.id.boton_contador);
        boton_contador.setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void actualizarFecha(Button btn){
        btn.setText(new Date().toString());
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.boton_fecha){
            actualizarFecha((Button) v);
        }
        else if(v.getId() == R.id.boton_contador){
            mContador++;
            ((Button) v).setText("Presionado: "+mContador+" veces");
        }
    }
}
