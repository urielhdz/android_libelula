package codigofacilito.com.rememberme;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.SendCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;


public class MyActivity extends ListActivity {
    private static final String KEY_FRIENDS_RELATION = "friendRelation";
    private static final String TAG = MyActivity.class.getSimpleName();
    List<ParseUser> mUsers;
    protected ParseRelation<ParseUser> mFriendRelation;
    protected ParseUser mCurrentUser;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        mCurrentUser = ParseUser.getCurrentUser();
        if(mCurrentUser == null) irLogin();
        else{
            mCurrentUser = ParseUser.getCurrentUser();
            mFriendRelation = mCurrentUser.getRelation(KEY_FRIENDS_RELATION);

            ParseQuery<ParseUser> query = ParseUser.getQuery();

            Intent intent = getIntent();
            String json_data = "{}";
            if(intent.getExtras() != null){
                json_data = intent.getExtras().getString("com.parse.Data");
            }

            try {
                JSONObject json = new JSONObject(json_data);
                String who_was = json.getString("phoneNumber");
                if(who_was != null){
                    Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                    smsIntent.setType("vnd.android-dir/mms-sms");
                    smsIntent.putExtra("address", who_was);
                    smsIntent.putExtra("sms_body","Hola, recibi tu mensaje, tenemos que vernos...");
                    startActivity(smsIntent);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }



    }
    @Override
    protected void onResume(){
        super.onResume();

        mCurrentUser = ParseUser.getCurrentUser();
        if(mCurrentUser != null){
            mFriendRelation = mCurrentUser.getRelation(KEY_FRIENDS_RELATION);

            ParseQuery<ParseUser> query = mFriendRelation.getQuery();

            query.findInBackground(new FindCallback<ParseUser>() {
                @Override
                public void done(List<ParseUser> parseUsers, ParseException e) {
                    if(e == null){
                        mUsers = parseUsers;
                        String[] usernames = new String[mUsers.size()];
                        int i = 0;
                        for(ParseUser user : mUsers){
                            usernames[i] = user.getUsername();
                            i++;
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(MyActivity.this,android.R.layout.simple_list_item_2,
                                android.R.id.text1, usernames);
                        setListAdapter(adapter);

                    }else{
                        AlertDialog.Builder builder = new AlertDialog.Builder(MyActivity.this);
                        builder.setMessage(e.getMessage());
                        builder.setTitle("Hubo un error");
                        builder.setPositiveButton(android.R.string.ok,null);
                        builder.create().show();
                    }
                }
            });
        }

    }
    private void irLogin(){
        Intent intent = new Intent(this,LogInActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
    @Override
    protected void onListItemClick(ListView l, View v, int position,long id){
        super.onListItemClick(l,v,position,id);

        ParseUser user = mUsers.get(position);
        ParseQuery pushQuery = ParseInstallation.getQuery();
        pushQuery.whereEqualTo("username",user.getUsername());
        Log.d(TAG,user.getUsername());
        /**/
        ParsePush push = new ParsePush();
        push.setQuery(pushQuery);

        try {
            JSONObject data = new JSONObject("{\"alert\": \"Soy " +mCurrentUser.getUsername()+", ¿me recuerdas?\",\"phoneNumber\": \""+mCurrentUser.getString("telefono")+"\"}");
            Log.d(TAG,"Data sent: "+data.toString());
            push.setData(data);
            push.sendInBackground(new SendCallback() {
                @Override
                public void done(ParseException e) {
                    Toast.makeText(MyActivity.this,"Tu mensaje ha sido enviado",Toast.LENGTH_LONG).show();
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        else if(id == R.id.action_logout){
            ParseUser.logOut();
            irLogin();
        }
        else if(id == R.id.action_friends){
            Intent intent = new Intent(this,EditFriends.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}
