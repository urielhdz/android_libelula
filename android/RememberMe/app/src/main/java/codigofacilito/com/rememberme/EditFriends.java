package codigofacilito.com.rememberme;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.List;


public class EditFriends extends ListActivity {
    private static final String KEY_FRIENDS_RELATION = "friendRelation";
    private static final String TAG = EditFriends.class.getSimpleName();
    List<ParseUser> mUsers;
    protected ParseRelation<ParseUser> mFriendRelation;
    protected ParseUser mCurrentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

    }

    @Override
    protected void onResume(){
        super.onResume();

        mCurrentUser = ParseUser.getCurrentUser();
        mFriendRelation = mCurrentUser.getRelation(KEY_FRIENDS_RELATION);

        ParseQuery<ParseUser> query = ParseUser.getQuery();

        query.findInBackground(new FindCallback<ParseUser>() {
            @Override
            public void done(List<ParseUser> parseUsers, ParseException e) {
                if(e == null){
                    mUsers = parseUsers;
                    String[] usernames = new String[mUsers.size()];
                    int i = 0;
                    for(ParseUser user : mUsers){
                        usernames[i] = user.getUsername();
                        i++;
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(EditFriends.this,android.R.layout.simple_list_item_checked,
                            android.R.id.text1, usernames);
                    setListAdapter(adapter);
                    addFriendsCheckmarks();
                    saveContactsAsFriends();
                }else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(EditFriends.this);
                    builder.setMessage(e.getMessage());
                    builder.setTitle("Hubo un error");
                    builder.setPositiveButton(android.R.string.ok,null);
                    builder.create().show();
                }
            }
        });
    }

    private void addFriendsCheckmarks() {
        mFriendRelation.getQuery().findInBackground(new FindCallback<ParseUser>() {
            @Override
            public void done(List<ParseUser> friends, ParseException e) {
                if(e == null){
                    for(int i = 0;i< mUsers.size();i++){
                        ParseUser user = mUsers.get(i);

                        for(ParseUser friend : friends){
                            if(friend.getObjectId().equals(user.getObjectId())){
                                getListView().setItemChecked(i,true);
                            }
                        }
                    }
                }else{
                    Log.e(TAG,e.getMessage());
                }
            }
        });
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position,long id){
        super.onListItemClick(l,v,position,id);
        //Already checked
        if(getListView().isItemChecked(position)){
            mFriendRelation.add(mUsers.get(position));
            saveUser();
        }
        else{
            mFriendRelation.remove(mUsers.get(position));
            saveUser();
        }

    }
    protected void saveUser(){
        mCurrentUser.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if(e != null) {
                    Log.e(TAG, e.getMessage());
                }
            }
        });
    }
    protected void checkIfContactExists(final String phoneNumber){
        String number = phoneNumber.replaceAll("[^0-9.]", "");
        Log.d(TAG,number);
        for(int i = 0;i< mUsers.size();i++){
            ParseUser user = mUsers.get(i);
            String user_phone = user.getString("telefono");
            if(user_phone != null ){

                if(user_phone.equals(number)){
                    Log.d(TAG,"El usuario se guardo por la libreta de contactos");
                    mFriendRelation.add(user);
                    saveUser();
                }

            }

        }

    }
    protected void saveContactsAsFriends(){

        Cursor cursor = null;
        try{
            cursor = EditFriends.this.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                    null,null,null,null);
            int phoneNumberIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            cursor.moveToFirst();
            do{
                String phoneNumber = cursor.getString(phoneNumberIdx);
                checkIfContactExists(phoneNumber);
            }while(cursor.moveToNext());
        }catch(Exception ex){
            ex.printStackTrace();
        }
        finally {
            if(cursor != null) cursor.close();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.edit_friends, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
