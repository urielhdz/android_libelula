package codigofacilito.com.rememberme;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class ReceptorMensaje extends BroadcastReceiver {
    private static final String TAG = ReceptorMensaje.class.getSimpleName();

    public ReceptorMensaje() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        try{
            String action = intent.getAction();
            JSONObject json_data = new JSONObject(intent.getExtras().getString("com.parse.Data"));
            Log.d(TAG,"JSON: "+json_data.toString() );
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
