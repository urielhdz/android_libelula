package codigofacilito.com.rememberme;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;


public class SignUpActivity extends Activity {
    protected EditText mUsername;
    protected EditText mPassword;
    protected EditText mEmail;
    protected EditText mPhone;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        ActionBar ab = getActionBar();
        ab.hide();

        mUsername = (EditText) findViewById(R.id.usernameField);
        mPassword = (EditText) findViewById(R.id.passwordField);
        mEmail = (EditText) findViewById(R.id.emailField);
        mPhone = (EditText) findViewById(R.id.phoneField);

        findViewById(R.id.openSignUpButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = mUsername.getText().toString().trim();
                String password = mPassword.getText().toString().trim();
                String email = mEmail.getText().toString().trim();
                String phone = mPhone.getText().toString().trim();

                if(username.isEmpty() ||password.isEmpty() || email.isEmpty() || phone.isEmpty()){
                    AlertDialog.Builder builder = new AlertDialog.Builder(SignUpActivity.this);
                    builder.setMessage("Asegúrate de llenar todos los campos.");
                    builder.setTitle("Hubo un error");
                    builder.setPositiveButton(android.R.string.ok,null);
                    builder.create().show();
                }
                else{
                    ParseUser usuario = new ParseUser();
                    usuario.setUsername(username);
                    usuario.setEmail(email);
                    usuario.setPassword(password);
                    usuario.put("telefono",phone);

                    usuario.signUpInBackground(new SignUpCallback() {
                        @Override
                        public void done(ParseException e) {
                            if(e == null){
                                saveInstallationData();

                            }
                            else{
                                AlertDialog.Builder builder = new AlertDialog.Builder(SignUpActivity.this);
                                builder.setMessage(e.getMessage());
                                builder.setTitle("Hubo un error");
                                builder.setPositiveButton(android.R.string.ok,null);
                                builder.create().show();
                            }
                        }
                    });
                }

            }
        });
    }
    protected void saveInstallationData(){
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.put("username",ParseUser.getCurrentUser().getUsername());
        installation.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                toMain();
            }
        });
    }
    public void toMain(){
        Intent intent = new Intent(SignUpActivity.this,MyActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.sign_up, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
