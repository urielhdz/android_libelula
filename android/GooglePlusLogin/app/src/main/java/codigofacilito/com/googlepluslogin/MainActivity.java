package codigofacilito.com.googlepluslogin;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;


public class MainActivity extends Activity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {
    private static final int REQUEST_CODE_RESOLVE_ERR = 9000;
    private static final int RC_SIGN_IN = 0;

    private ConnectionResult mConnectionResult;
    private GoogleApiClient mGoogleApliClient;
    private boolean mSignInClicked;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.sign_in_button).setOnClickListener(this);
        mGoogleApliClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .build();

    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApliClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(mGoogleApliClient.isConnected())
            mGoogleApliClient.disconnect();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConnected(Bundle bundle) {
        //String accountName = Toast.makeText(this, accountName + " is connected.", Toast.LENGTH_LONG).show();
        Toast.makeText(this,"Yei",Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApliClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        mConnectionResult = result;
        if(mSignInClicked)
            resolveSigInErrors();
    }
    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent intent) {
        if (requestCode == RC_SIGN_IN) {
            if(!mGoogleApliClient.isConnecting()) mGoogleApliClient.connect();
        }
    }
    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.sign_in_button && !mGoogleApliClient.isConnecting()){

            mSignInClicked = true;
            resolveSigInErrors();
        }
    }

    private void resolveSigInErrors() {
        if(mConnectionResult.hasResolution()){

            try {
                startIntentSenderForResult(mConnectionResult.getResolution().getIntentSender(),RC_SIGN_IN,null,0,0,0);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
                mGoogleApliClient.connect();
            }
        }
        else {
            // Show dialog using GooglePlayServicesUtil.getErrorDialog()
            GooglePlayServicesUtil.showErrorDialogFragment(mConnectionResult.getErrorCode(),this,RC_SIGN_IN);

        }
    }
}
