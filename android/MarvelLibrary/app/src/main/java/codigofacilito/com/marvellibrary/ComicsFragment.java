package codigofacilito.com.marvellibrary;



import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 *
 */
public class ComicsFragment extends ListFragment {

    protected ArrayList<HashMap<String,String>> mSuperHeroes = new ArrayList<HashMap<String,String>>();



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        new ObtenerSuperHeroes().execute("");
        return inflater.inflate(R.layout.fragment_comics, container, false);

    }
    private class ObtenerSuperHeroes extends AsyncTask<String,Void,JSONArray> {
    /*
    * ts - a timestamp (or other long string which can change on a request-by-request basis)
    * hash - a md5 digest of the ts parameter, your private key and your public key (e.g. md5(ts+privateKey+publicKey)
    * */

        private final String LOG_TAG = ObtenerSuperHeroes.class.getSimpleName();
        @Override
        protected JSONArray doInBackground(String... params) {
            String end_point = params[0];
            Long timestamp_long =(System.currentTimeMillis()/1000);
            String fecha = timestamp_long.toString();
            String api_key = "205530edd3fa697705aa1a2430ae87ba";
            String private_key = "5cba4ac12e3fb326645e2f1cfadeae643aa20132";
            String md5_string = Utilidades.md5(fecha+""+private_key+""+api_key);
            String final_string;
            int responseCode = -1;

            try{

                Uri builtUri = Uri.parse("http://gateway.marvel.com/v1/public/comics?").buildUpon()
                        .appendQueryParameter("apikey", api_key)
                        .appendQueryParameter("ts", fecha)
                        .appendQueryParameter("limit","40")
                        .appendQueryParameter("hash", md5_string).build();
                String final_url = builtUri.toString();
                Log.w(LOG_TAG, final_url);
                URL data_url = new URL(final_url);
                HttpURLConnection connection = (HttpURLConnection) data_url.openConnection();
                connection.setRequestMethod("GET");
                connection.connect();
                //Recibimos la respuesta
                responseCode = connection.getResponseCode();
                //Verificamos que fue correcta
                Log.w(LOG_TAG,responseCode+"");

                //Clase padre para cualquier lector de bytes
                InputStream inputStream = connection.getInputStream();
                //InputStreamReader transforma los datos en bytes a caracteres
                //Reader la clase base de cualquier lector de caracteres
                StringBuffer buffer = new StringBuffer();
                if (inputStream == null) {
                    // Nothing to do.
                    final_string = null;
                }
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                    // But it does make debugging a *lot* easier if you print out the completed
                    // buffer for debugging.
                    buffer.append(line + "\n");
                }

                if (buffer.length() == 0) {
                    // Stream was empty.  No point in parsing.
                    final_string = null;
                }
                final_string = buffer.toString();
                Log.w(LOG_TAG, final_string.toString());

                JSONObject data_object =  new JSONObject(final_string);

                return data_object.getJSONObject("data").getJSONArray("results");

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }
        @Override
        protected void onPostExecute(JSONArray result){

            try{
                updateList(result);
            }catch (JSONException e){
                e.printStackTrace();
            }
        }
    }
    public void updateList(JSONArray json_data) throws JSONException {
        if(json_data == null){
        }else{
            HashMap<String,String> hash_for_adapter = new HashMap<String, String>();
            for (int i = 0;i<json_data.length();i++){
                JSONObject super_heroe = json_data.getJSONObject(i);
                String title = super_heroe.getString("title");
                String descripcion = super_heroe.getString("description");
                hash_for_adapter = new HashMap<String, String>();
                hash_for_adapter.put("titulo",title);
                hash_for_adapter.put("descripcion",descripcion);

                this.mSuperHeroes.add(hash_for_adapter);
            }

            String[] keys = {"titulo","descripcion"};
            int[] ids = {android.R.id.text1,android.R.id.text2};

            SimpleAdapter adapter = new SimpleAdapter(getListView().getContext(),this.mSuperHeroes, android.R.layout.two_line_list_item,keys,ids);

            setListAdapter(adapter);

        }
    }

}
