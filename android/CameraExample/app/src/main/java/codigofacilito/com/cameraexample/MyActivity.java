package codigofacilito.com.cameraexample;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class MyActivity extends Activity {
    public static final int foto_request = 0;
    public static final int video_request = 1;
    public static final int foto_gallery_request = 2;
    public static final int video_gallery_request = 3;

    public static final int MEDIA_TYPE_IMAGE = 4;
    public static final int MEDIA_TYPE_VIDEO = 5;
    private final String TAG = MyActivity.class.getSimpleName();

    protected Uri mMediaUri ; // Uniform Resource identifier

    protected DialogInterface.OnClickListener mDialogListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            //Which elemento seleccionado
            switch (which){
                case 0:
                    Intent fotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    //El activity debera regresar un resultado
                    mMediaUri = getOutputMediaFileUri(MEDIA_TYPE_VIDEO);
                    if(mMediaUri == null){
                        Toast.makeText(MyActivity.this,R.string.error_sdcard,Toast.LENGTH_LONG).show();
                    }
                    else{
                        fotoIntent.putExtra(MediaStore.EXTRA_OUTPUT,mMediaUri);
                        startActivityForResult(fotoIntent, foto_request);
                    }
                    break;
                case 1:
                    Intent videoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                    //El activity debera regresar un resultado
                    mMediaUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
                    if(mMediaUri == null){
                        Toast.makeText(MyActivity.this,R.string.error_sdcard,Toast.LENGTH_LONG).show();
                    }
                    else{
                        videoIntent.putExtra(MediaStore.EXTRA_OUTPUT,mMediaUri);
                        videoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT,10); //Limitar duracion
                        videoIntent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY,0); // 0 lowest queality, 1 highest quality
                        startActivityForResult(videoIntent, video_request);
                    }
                    break;
                case 2:
                    Intent elegirFoto = new Intent(Intent.ACTION_GET_CONTENT);
                    elegirFoto.setType("image/*"); // Filtrar imagenes
                    startActivityForResult(elegirFoto,foto_gallery_request);
                    break;
                case 3:
                    break;
            }
        }
        private Uri getOutputMediaFileUri(int mediaTypeImage) {
            //Debemos validar que el SDCard esta montado
            if(isSDCardAvailable()){
                // get the URI and returned
                // 1.- Get External Storage directory
                String appName = MyActivity.this.getString(R.string.app_name);
                File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),appName);
                // 2.- Create our own subdirectory
                if(! mediaStorageDir.exists()){
                    if (mediaStorageDir.mkdirs()) return  null; // Returns boolean if it fails
                }
                // 3.- Create filename
                // 4.- Create the actual file
                File mediaFile;
                Date now = new Date(); // Current date
                String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(now);
                String path = mediaStorageDir.getPath() + File.separator; // File separator give us the OS separator
                if(mediaTypeImage == MEDIA_TYPE_IMAGE){
                    mediaFile = new File(path+"IMG_"+timestamp+".jpg");
                }
                else if(mediaTypeImage == MEDIA_TYPE_VIDEO){
                    mediaFile = new File(path+"VID_"+timestamp+".mp4");
                }
                else return null;
                Log.d(TAG,"File: "+Uri.fromFile(mediaFile));

                // 5.- Return the file URI
                return Uri.fromFile(mediaFile);
            }
            else
                return null;
        }
        private boolean isSDCardAvailable(){
            String state = Environment.getExternalStorageState();
            if(state.equals(Environment.MEDIA_MOUNTED)){
                return true;
            }
            return false;
        }
    };



    //<users-permission android:name="android.permission.CAMERA"/>
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
    }

    //Se ejecuta cuando regresamos de la camara
    @Override
    protected void onActivityResult(int requestCode,int resultCode,Intent data){
        //El primero lo pasamos, el segundo es el resultado, el tercero representa datos opcionales
        super.onActivityResult(requestCode,resultCode,data);

        if(resultCode == RESULT_OK){
            //Agregar a la galeria
            if(requestCode == foto_gallery_request || requestCode == video_gallery_request){
                if(data == null){
                    Toast.makeText(this,"Hubo un error",Toast.LENGTH_LONG).show();
                }
                else{
                    mMediaUri = data.getData();
                }
            }
            else
            {
                Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                mediaScanIntent.setData(mMediaUri);
                //No abre la galería, hace broadcast del intent, registra el archivo para verse en la galería
                sendBroadcast(mediaScanIntent);
            }

        }
        else if(resultCode != RESULT_CANCELED){
            Toast.makeText(this,"Hubo un error",Toast.LENGTH_LONG).show();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        else if(id == R.id.action_camera){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setItems(R.array.camera_choices, mDialogListener);
            AlertDialog dialog = builder.create();
            dialog.show();
        }
        return super.onOptionsItemSelected(item);
    }
}
