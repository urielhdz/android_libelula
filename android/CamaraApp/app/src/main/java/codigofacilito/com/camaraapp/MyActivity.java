package codigofacilito.com.camaraapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class MyActivity extends Activity {
    protected Uri mMediaUri;
    public static final int foto_request = 0;
    public static final int  TIPO_IMAGEN = 4;

    private final String TAG = MyActivity.class.getSimpleName();
    protected DialogInterface.OnClickListener mDialogListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which){
                case 0:
                    Intent fotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    mMediaUri = getMediaFile(TIPO_IMAGEN);
                    if(mMediaUri == null){
                        Toast.makeText(MyActivity.this,"Hubo un error al guardar",Toast.LENGTH_LONG).show();
                    }else{
                        fotoIntent.putExtra(MediaStore.EXTRA_OUTPUT,mMediaUri);
                        startActivityForResult(fotoIntent,foto_request);
                    }

            }
        }
        private Uri getMediaFile(int tipo_archivo){
            if(hayAlmacenamientoExterno()){
                String appName = MyActivity.this.getString(R.string.app_name);
                File carpetaMedia = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),appName);
                if(!carpetaMedia.exists()){
                    if(carpetaMedia.mkdirs()) return null;
                }
                File archivo;
                Date ahora = new Date();
                String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(ahora);
                String path = carpetaMedia.getPath() + File.separator;

                if(tipo_archivo == TIPO_IMAGEN){
                    archivo = new File(path+"IMG"+timestamp+".jpg");
                }
                else return null;
                Log.d(TAG,Uri.fromFile(archivo).toString());

                return Uri.fromFile(archivo);
            }
            else
                return null;
        }
        private boolean hayAlmacenamientoExterno(){
           String estado_sdcard = Environment.getExternalStorageState();
           if(estado_sdcard.equals(Environment.MEDIA_MOUNTED)){
                return true;
           }
           return false;
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(resultCode == RESULT_OK){
            Intent guardarArchivoIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            guardarArchivoIntent.setData(mMediaUri);
            sendBroadcast(guardarArchivoIntent);
        }
        else
            Toast.makeText(this,"No se guardo la foto: "+requestCode,Toast.LENGTH_LONG).show();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        else if(id == R.id.action_camera){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setItems(R.array.opciones_camara,mDialogListener);
            AlertDialog dialog = builder.create();
            dialog.show();
        }
        return super.onOptionsItemSelected(item);
    }
}
