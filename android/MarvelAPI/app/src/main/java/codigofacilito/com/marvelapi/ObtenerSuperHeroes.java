package codigofacilito.com.marvelapi;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;

/**
 * Created by urielhernandez on 19/08/14.
 */
public class ObtenerSuperHeroes extends AsyncTask<String,Void,JSONArray> {
    /*
    * ts - a timestamp (or other long string which can change on a request-by-request basis)
    * hash - a md5 digest of the ts parameter, your private key and your public key (e.g. md5(ts+privateKey+publicKey)
    * */

    @Override
    protected JSONArray doInBackground(String... params) {
        String end_point = params[0];
        String fecha = new Date().toString();
        String api_key = "205530edd3fa697705aa1a2430ae87ba";
        String private_key = "5cba4ac12e3fb326645e2f1cfadeae643aa20132";
        String md5_string = Utilidades.md5(fecha+""+private_key+""+api_key);
        String final_string;
        int responseCode = -1;

        try{
            String final_url = "http://gateway.marvel.com/v1/public/characters?t="+fecha+"&apikey="+api_key+"&hash="+md5_string;
            URL data_url = new URL(final_url);
            HttpURLConnection connection = (HttpURLConnection) data_url.openConnection();
            connection.connect();
            //Recibimos la respuesta
            responseCode = connection.getResponseCode();
            //Verificamos que fue correcta
            if(responseCode == HttpURLConnection.HTTP_OK){
                //Clase padre para cualquier lector de bytes
                InputStream inputStream = connection.getInputStream();
                //InputStreamReader transforma los datos en bytes a caracteres
                //Reader la clase base de cualquier lector de caracteres
                StringBuffer buffer = new StringBuffer();
                if (inputStream == null) {
                    // Nothing to do.
                    final_string = null;
                }
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                    // But it does make debugging a *lot* easier if you print out the completed
                    // buffer for debugging.
                    buffer.append(line + "\n");
                }

                if (buffer.length() == 0) {
                    // Stream was empty.  No point in parsing.
                    final_string = null;
                }
                final_string = buffer.toString();


                JSONObject data_object =  new JSONObject(final_string);
                Log.v("ObtenerSuperHeroes",data_object.toString());
                return data_object.getJSONObject("data").getJSONArray("result");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
