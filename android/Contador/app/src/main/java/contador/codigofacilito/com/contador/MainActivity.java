package contador.codigofacilito.com.contador;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import java.util.Date;

public class MainActivity extends Activity implements View.OnClickListener {
    int mcounter = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btn = (Button) findViewById(R.id.date_button);
        btn.setOnClickListener(this);
        updateTime(btn);
        Button counter = (Button) (Button) findViewById(R.id.counter_button);
        counter.setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    public String getTime(){
        return new Date().toString();
    }
    public void updateTime(Button btn){
        btn.setText(getTime());
    }
    public void updateCounter(Button btn){
        mcounter++;
        btn.setText("Presionado:" +mcounter+" veces");
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

        if(v.getId() == R.id.counter_button){
            updateCounter(((Button) v));

        }
        else if(v.getId() == R.id.date_button){
            updateTime(((Button) v));
        }
    }
}
