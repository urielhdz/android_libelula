package codigofacilito.com.validador;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends Activity {
    EditText mNumero;
    Button mBoton;
    private static final String TAG = "Validador.MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNumero = (EditText) findViewById(R.id.number_field);
        mBoton = (Button) findViewById(R.id.boton_validar);

        mBoton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String numero = mNumero.getText().toString();
                Log.d(TAG,"El valor ingresado es: "+numero);
                if(esNumerico(numero)){
                    LinearLayout ll = (LinearLayout) findViewById(R.id.layout);
                    TextView label = new TextView(MainActivity.this);
                    label.setText("Era un número");
                    label.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                              ViewGroup.LayoutParams.WRAP_CONTENT
                            ));
                    ll.addView(label);
                }
                else{
                    Toast toast = Toast.makeText(getApplicationContext(),
                            "No insertaste un número",Toast.LENGTH_LONG);
                    toast.show();
                }
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public boolean esNumerico(String texto){
        return texto.matches("-?\\d+(\\.\\d+)?");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
